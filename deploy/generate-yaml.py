#Import necessary functions from Jinja2 module
from jinja2 import Environment, FileSystemLoader

#Import YAML module
import yaml
import os

stage = os.getenv('CI_JOB_STAGE', 'deploy-dev')
stage = stage.split("-")[1]
#os.putenv('KUBE_TARGET_ENV',stage)
config_data = yaml.load(open('./' + stage + '.yml'))
#change the generated yaml even when the docker image tag has not changed
config_data.update({'pipelineId': os.getenv("CI_PIPELINE_ID")})

#Load Jinja2 template
env = Environment(loader = FileSystemLoader('./templates'), trim_blocks=True, lstrip_blocks=True)

template = env.get_template('deployment.yaml.j2')
file = open("deployment.yaml","w")
file.write(template.render(config_data))

template = env.get_template('service.yaml.j2')
file = open("service.yaml","w")
file.write(template.render(config_data))

