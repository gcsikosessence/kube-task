#!/bin/bash

set -eox pipefail

echo $GCLOUD_AUTH_JSON | base64 -d > /tmp/auth.json
gcloud auth activate-service-account --key-file=/tmp/auth.json
gcloud config set project hale-lantern-221421
gcloud container clusters get-credentials test-gke-cluster-py --zone europe-west1-b

cd /deploy
python generate-yaml.py

kubectl apply -f /deploy/deployment.yaml
kubectl apply -f /deploy/service.yaml
