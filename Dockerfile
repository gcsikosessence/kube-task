FROM golang:1.11.1-alpine3.8 as build-env

RUN apk update; \
    apk add --no-cache \
        ca-certificates \
        curl \
        git \
        make \
        openssl; \
    rm -rf /var/cache/apk/*; \
    rm -rf /tmp/*;

ADD . /go/src/gitlab.com/gcsikosessence/kube-task
RUN cd /go/src/gitlab.com/gcsikosessence/kube-task/service && go get ./... && CGO_ENABLED=0 go build -o kube-task .

FROM scratch
COPY --from=build-env /go/src/gitlab.com/gcsikosessence/kube-task/service/kube-task /
ENTRYPOINT [ "/kube-task" ]
