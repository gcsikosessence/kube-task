#!/bin/bash

set -eox pipefail

echo $GCLOUD_AUTH_JSON | base64 -d > /tmp/auth.json
gcloud auth activate-service-account --key-file=/tmp/auth.json
gcloud config set project hale-lantern-221421
gcloud container clusters get-credentials test-gke-cluster-py --zone europe-west1-b

export EXTERNAL_IP=$(kubectl get svc -n dev kube-task-service -o jsonpath='{$.status.loadBalancer.ingress.*.ip}')

endpoints=( / /pods /status )
for i in "${endpoints[@]}"
do
   : 
   echo "calling ${i}"
   if [ $(curl -sL -w "%{http_code}\\n" $EXTERNAL_IP${i} -o /dev/null) -ne 200 ]; then exit 1; fi
done